const limitFunctionCallCount = require('../limitFunctionCallCount.cjs');

function callback(x,y){
    return x*y;
}
const result = limitFunctionCallCount(callback,5);
console.log(result(2,3));
console.log(result(6,3));
console.log(result(9,3));
console.log(result(1,4));
console.log(result(7,9));
console.log(result(2,2));
