const cacheFunction = require('../cacheFunction.cjs');
const result = cacheFunction((parameter1, parameter2) => 
    parameter1 % parameter2
);

console.log(result(4, 8));
console.log(result(8, 3));
console.log(result(9, 6));
console.log(result(4, 8));