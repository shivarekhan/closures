function limitFunctionCallCount(callback, upperLimitOfInvocation) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned
    if (typeof callback !== 'function' || typeof upperLimitOfInvocation !== 'number') {
        return null;
    }
    let invkoingFrequency = 0;
    return function () {
        if (invkoingFrequency < upperLimitOfInvocation) {
            invkoingFrequency++;
            if (typeof callback(...arguments) === "undefined") {
                return null;
            }
            return callback(...arguments);
        }
        return [];
    };

}
module.exports = limitFunctionCallCount;