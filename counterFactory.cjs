function counterFactory() {
    // Return an object that has two methods called `increment` and `decrement`.
    // `increment` should increment a counter variable in closure scope and return it.
    // `decrement` should decrement the counter variable and return it.
    let frequencyCounter = 0;
    function increment() {
        frequencyCounter += 1;
        return frequencyCounter;
    }
    function decrement() {
        frequencyCounter -= 1;
        return frequencyCounter;
    }
    return { increment, decrement };
}
module.exports = counterFactory;